import 'package:flutter/material.dart';
import 'pong.dart';
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      title: 'Pong Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Game pong'),
        ),
        body: const SafeArea(
            child: Pong()
        ),
      ),
    );
  }
}
